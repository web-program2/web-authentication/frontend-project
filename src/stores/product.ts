import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useProductStore = defineStore("product", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();

  const dialog = ref(false);
  const delDialog = ref(false);
  const products = ref<Product[]>([]);
  const editedProduct = ref<Product>({ name: "", price: 0 });

  watch(dialog, (newDialog) => {
    if (!newDialog) {
      editedProduct.value = { name: " ", price: 0 };
    }
  });

  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts();
      products.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("Failed to load resource.");
    }
    loadingStore.isLoading = false;
  }
  async function saveProduct() {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        await productService.saveProduct(editedProduct.value);
      }
      dialog.value = false;
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("Failed to save product");
    }
    loadingStore.isLoading = false;
  }

  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }

  async function deleteProduct(id: number) {
    loadingStore.isLoading = true;
    delDialog.value = true;
    try {
      await productService.deleteProduct(id);
      delDialog.value = false;

      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("Failed to delete product");
    }
    loadingStore.isLoading = false;
  }
  return {
    products,
    getProducts,
    dialog,
    editedProduct,
    saveProduct,
    editProduct,
    deleteProduct,
    delDialog,
  };
});
